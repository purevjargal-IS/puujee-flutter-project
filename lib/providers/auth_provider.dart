// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthProvider with ChangeNotifier {
  bool _isLoggedIn = false;
  String _accessToken = '';
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  bool get isLoggedIn => _isLoggedIn;
  String get accessToken => _accessToken;

  AuthProvider() {
    _loadAccessToken();
  }

  Future<void> _loadAccessToken() async {
    _accessToken = await _secureStorage.read(key: 'access_token') ?? '';
    _isLoggedIn = _accessToken.isNotEmpty;
    notifyListeners();
  }

  Future<void> login(String token) async {
    await _secureStorage.write(key: 'access_token', value: token);
    _accessToken = token;
    _isLoggedIn = true;
    notifyListeners();
  }

  Future<void> logout() async {
    await _secureStorage.delete(key: 'access_token');
    _accessToken = '';
    print('Access TOKEN: $_accessToken');
    _isLoggedIn = false;
    notifyListeners();
  }
}
