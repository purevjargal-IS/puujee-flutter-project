// ignore_for_file: avoid_print, use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_application_1/loan/home.dart';
import 'package:flutter_application_1/loan/register.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  String? _validatePhone(String? value) {
    if (value == null || value.isEmpty) {
      return 'Хэрэглэгчийн нэрээ оруулна уу!';
    }
    return null;
  }

  String? _validatePassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'Нууц үгээ оруулна уу!';
    }
    return null;
  }

  Future<void> _getToken(String username, String password) async {
    try {
      final response = await http.post(
        Uri.parse('https://system.edata.mn/ords/monterosa/public/get_token'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'username': username,
          'password': password,
        }),
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        var token = data['token'];
        print('Access token: $token');
        await _secureStorage.write(key: 'access_token', value: token);
        Navigator.push(context,
            MaterialPageRoute(builder: ((context) => const HomeScreen())));
      } else {
        print('Токен авч чадсангүй.');
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Нэвтэрч чадсангүй, дахин оролдоно уу.'),
          ),
        );
      }
    } catch (e) {
      print('Exception occurred: $e');
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Алдаа гарлаа, дахин оролдоно уу.'),
        ),
      );
    }
  }

  Future<void> _getAndReg(String username, String password) async {
    try {
      final response = await http.post(
        Uri.parse('https://system.edata.mn/ords/monterosa/public/get_token'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'username': username,
          'password': password,
        }),
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        var token = data['token'];
        print('Access token: $token');
        await _secureStorage.write(key: 'access_token', value: token);
        Navigator.push(context,
            MaterialPageRoute(builder: ((context) => const RegisterScreen())));
      } else {
        print('Токен авч чадсангүй.');
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Нэвтэрч чадсангүй, дахин оролдоно уу.'),
          ),
        );
      }
    } catch (e) {
      print('Exception occurred: $e');
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Алдаа гарлаа, дахин оролдоно уу.'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromARGB(255, 255, 255, 255),
              Color(0xFF99FF66),
            ],
          ),
        ),
        child: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.mobile_friendly_rounded,
                  size: MediaQuery.of(context).size.height * 0.2,
                  color: Colors.lightGreen[700],
                ),
                const SizedBox(height: 30),
                const Text(
                  'Байгууллагын нэр',
                  style: TextStyle(
                    color: Color(0xFF416D19),
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 30),
                Container(
                  padding: const EdgeInsets.all(30.0),
                  height: 450,
                  width: 320,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10,
                        offset: Offset(0, 5),
                      ),
                    ],
                  ),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextFormField(
                          controller: _usernameController,
                          decoration: const InputDecoration(
                            labelText: 'Хэрэглэгчийн нэр',
                            labelStyle: TextStyle(color: Color(0xFF416D19)),
                          ),
                          keyboardType: TextInputType.phone,
                          validator: _validatePhone,
                        ),
                        const SizedBox(height: 20),
                        TextFormField(
                          controller: _passwordController,
                          decoration: const InputDecoration(
                            labelText: 'Нууц үг',
                            labelStyle: TextStyle(color: Color(0xFF416D19)),
                          ),
                          keyboardType: TextInputType.phone,
                          validator: _validatePassword,
                        ),
                        const SizedBox(height: 70),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: const BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                blurRadius: 20,
                                offset: Offset(0, 5),
                              ),
                            ],
                          ),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Түр хүлээнэ үү...'),
                                        CircularProgressIndicator(),
                                      ],
                                    ),
                                  ),
                                );
                                _getToken(_usernameController.text,
                                    _passwordController.text);
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFF99FF66),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                            ),
                            child: const Text(
                              'Нэвтрэх',
                              style: TextStyle(
                                color: Color(0xFF416D19),
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 30),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          decoration: const BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                blurRadius: 20,
                                offset: Offset(0, 5),
                              ),
                            ],
                          ),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(
                                    content: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text('Түр хүлээнэ үү...'),
                                        CircularProgressIndicator(),
                                      ],
                                    ),
                                  ),
                                );
                                _getAndReg(_usernameController.text,
                                    _passwordController.text);
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFF99FF66),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                            ),
                            child: const Text(
                              'Бүртгүүлэх',
                              style: TextStyle(
                                color: Color(0xFF416D19),
                                fontSize: 20,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
