// ignore_for_file: avoid_print, use_build_context_synchronously

import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/loan/profile_see.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:mime/mime.dart';
import '../layout/footer.dart';

class ProfileEditScreen extends StatefulWidget {
  const ProfileEditScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _ProfileEditScreenState createState() => _ProfileEditScreenState();
}

class _ProfileEditScreenState extends State<ProfileEditScreen> {
  // ignore: unused_field
  final _formKey = GlobalKey<FormState>();
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();
  final TextEditingController _userID = TextEditingController();
  final TextEditingController _username = TextEditingController();
  final TextEditingController _lastname = TextEditingController();
  final TextEditingController _firstname = TextEditingController();
  final TextEditingController _regno = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _bankAN = TextEditingController();
  int _selectedIndex = 0;
  XFile? _imageFile;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<void> _pickImage() async {
    final imagePicker = ImagePicker();
    final pickedFile = await imagePicker.pickImage(
      source: ImageSource.gallery,
    );

    if (pickedFile != null) {
      setState(() {
        _imageFile = pickedFile;
      });
    }
  }

  Future<void> _takeImage() async {
    final imagePicker = ImagePicker();
    final pickedFile = await imagePicker.pickImage(
      source: ImageSource.camera,
    );

    if (pickedFile != null) {
      setState(() {
        _imageFile = pickedFile;
      });
    }
  }

  Future<void> _uploadFile() async {
    if (_imageFile == null) return;

    String apiUrl =
        'https://system.edata.mn/ords/monterosa/loan/user_pic/${_userID.text}';
    List<int> fileBytes = await _imageFile!.readAsBytes();
    String mimeType =
        lookupMimeType(_imageFile!.path) ?? 'application/octet-stream';

    try {
      var request = http.MultipartRequest('POST', Uri.parse(apiUrl));
      request.headers['Content-Type'] = 'multipart/form-data';
      request.files.add(http.MultipartFile.fromBytes(
        'L_USERPIC',
        fileBytes,
        filename: _imageFile!.path.split('/').last,
        contentType: MediaType.parse(mimeType),
      ));

      http.StreamedResponse response = await request.send();
      var responseData = await http.Response.fromStream(response);

      if (response.statusCode == 200) {
        print('File uploaded successfully');
      } else {
        print('File upload failed with status: ${response.statusCode}');
        print('Response body: ${responseData.body}');
      }
    } catch (e) {
      print('Error uploading file: $e');
    }
  }

  Future<void> _editUserInfo() async {
    String? accessToken = await _secureStorage.read(key: 'access_token');

    if (accessToken != null) {
      final response = await http.put(
        Uri.parse(
            'https://system.edata.mn/ords/monterosa/loan/loan_user/${_userID.text}'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $accessToken',
        },
        body: jsonEncode(<String, dynamic>{
          "l_user_id": _userID.text,
          "l_username": _username.text,
          "l_lastname": _lastname.text,
          "l_firstname": _firstname.text,
          "l_regnumber": _regno.text,
          "l_phone": _phone.text,
          "l_bank_an": _bankAN.text,
        }),
      );

      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Successful!')),
        );

        // showDialog(context: context, builder: (BuildContext context) {
        //   return AlertDialog(
        //     // content: '',
        //   );
        // });

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const ProfileScreen()),
        );
        print(response.body);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Failed with status: ${response.statusCode}')),
        );
        print('Failed with status: ${response.statusCode}');
        print(response.body);
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Access token not found')),
      );
    }
  }

  // void _back() {
  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(builder: (context) => const ProfileScreen()),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Footer(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xFFFFFFFF),
                    Color(0xFF99FF66),
                  ],
                ),
              ),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.8,
              margin: const EdgeInsets.only(top: 100.0),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (_imageFile != null)
                          Container(
                            width: 120.0,
                            height: 120.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                color: Colors.white,
                                width: 5.0,
                              ),
                            ),
                            child: CircleAvatar(
                              radius: 50,
                              backgroundImage:
                                  FileImage(File(_imageFile!.path)),
                            ),
                          ),
                        const SizedBox(width: 20),
                        Column(
                          children: [
                            ElevatedButton(
                              onPressed: _takeImage,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: const Color(0xFF416D19),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                                foregroundColor: Colors.white,
                              ),
                              child: const Text('Зураг дарах'),
                            ),
                            const SizedBox(height: 20),
                            ElevatedButton(
                              onPressed: _pickImage,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: const Color(0xFF416D19),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                                foregroundColor: Colors.white,
                              ),
                              child: const Text('Зураг сонгох'),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 30),
                    TextFormField(
                      controller: _userID,
                      decoration: const InputDecoration(
                        labelText: 'Хэрэглэгчийн ID',
                        labelStyle: TextStyle(color: Color(0xFF416D19)),
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _username,
                      decoration: const InputDecoration(
                        labelText: 'Хэрэглэгчийн нэр',
                        labelStyle: TextStyle(color: Color(0xFF416D19)),
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _lastname,
                      decoration: const InputDecoration(
                        labelText: 'Овог',
                        labelStyle: TextStyle(color: Color(0xFF416D19)),
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _firstname,
                      decoration: const InputDecoration(
                        labelText: 'Нэр',
                        labelStyle: TextStyle(color: Color(0xFF416D19)),
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _regno,
                      decoration: const InputDecoration(
                        labelText: 'РД',
                        labelStyle: TextStyle(color: Color(0xFF416D19)),
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _phone,
                      decoration: const InputDecoration(
                        labelText: 'Утас',
                        labelStyle: TextStyle(color: Color(0xFF416D19)),
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                    const SizedBox(height: 20),
                    TextFormField(
                      controller: _bankAN,
                      decoration: const InputDecoration(
                        labelText: 'Дансны дугаар',
                        labelStyle: TextStyle(color: Color(0xFF416D19)),
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                    const SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            width: 250,
                            child: ElevatedButton(
                              onPressed: _uploadFile,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: const Color(0xFF416D19),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 10),
                              ),
                              child: const Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'Зураг баталгаажуулах',
                                  style: TextStyle(color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 20),
                          SizedBox(
                            width: 250,
                            child: ElevatedButton(
                              onPressed: _editUserInfo,
                              style: ElevatedButton.styleFrom(
                                backgroundColor: const Color(0xFF416D19),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 30, vertical: 10),
                              ),
                              child: const Align(
                                alignment: Alignment.center,
                                child: Text(
                                  'Хадгалах',
                                  style: TextStyle(color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),

                          // ElevatedButton(
                          //   onPressed: _back,
                          //   style: ElevatedButton.styleFrom(
                          //     backgroundColor: const Color(0xFF416D19),
                          //     padding: const EdgeInsets.symmetric(
                          //         horizontal: 30, vertical: 10),
                          //   ),
                          //   child: const Text(
                          //     'Буцах',
                          //     style: TextStyle(color: Colors.white),
                          //   ),
                          // ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
