// ignore_for_file: library_private_types_in_public_api, use_build_context_synchronously, avoid_print

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_application_1/loan/login.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _regno = TextEditingController();
  final TextEditingController _bankAN = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  String? _validatePassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'Нууц үгээ давтана уу!';
    } else if (_password.text != _confirmPassword.text) {
      return 'Нууц үгээ шалгана уу!';
    }
    return null;
  }

  Future<void> _insert() async {
    if (_formKey.currentState!.validate()) {
      String? accessToken = await _secureStorage.read(key: 'access_token');

      if (accessToken != null) {
        final response = await http.post(
          Uri.parse('https://system.edata.mn/ords/monterosa/loan/loan_user'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, String>{
            "l_regnumber": _regno.text,
            "l_bank_an": _bankAN.text,
            "l_password": _password.text,
          }),
        );

        if (response.statusCode == 200) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Successfully registered!')),
          );
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const LoginScreen()),
          );
          print(response.body);
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Failed to register')),
          );
          print(response.body);
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Access token not found!')),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color.fromARGB(255, 255, 255, 255),
              Color(0xFF99FF66),
            ],
          ),
        ),
        child: Center(
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.mobile_friendly_rounded,
                  size: size.height * 0.2,
                  color: Colors.lightGreen[700],
                ),
                const SizedBox(height: 30),
                const Text(
                  'Байгууллагын нэр',
                  style: TextStyle(
                    color: Color(0xFF416D19),
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 30),
                FractionallySizedBox(
                  widthFactor: 0.85,
                  child: Container(
                    padding: const EdgeInsets.all(30.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 10,
                          offset: Offset(0, 5),
                        ),
                      ],
                    ),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          TextFormField(
                            controller: _regno,
                            decoration: const InputDecoration(
                              labelText: 'РД',
                              labelStyle: TextStyle(color: Color(0xFF416D19)),
                            ),
                            keyboardType: TextInputType.phone,
                          ),
                          const SizedBox(height: 20),
                          TextFormField(
                            controller: _bankAN,
                            decoration: const InputDecoration(
                              labelText: 'Дансны дугаар',
                              labelStyle: TextStyle(color: Color(0xFF416D19)),
                            ),
                            keyboardType: TextInputType.phone,
                          ),
                          const SizedBox(height: 20),
                          TextFormField(
                            controller: _password,
                            decoration: const InputDecoration(
                              labelText: 'Нууц үг',
                              labelStyle: TextStyle(color: Color(0xFF416D19)),
                            ),
                            keyboardType: TextInputType.phone,
                            obscureText: true,
                          ),
                          const SizedBox(height: 20),
                          TextFormField(
                            controller: _confirmPassword,
                            validator: _validatePassword,
                            decoration: const InputDecoration(
                              labelText: 'Нууц үг давтах',
                              labelStyle: TextStyle(color: Color(0xFF416D19)),
                            ),
                            keyboardType: TextInputType.phone,
                            obscureText: true,
                          ),
                          const SizedBox(height: 20),
                          ElevatedButton(
                            onPressed: _insert,
                            style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFF99FF66),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 30, vertical: 10),
                            ),
                            child: const Text(
                              'Хадгалах',
                              style: TextStyle(color: Color(0xFF416D19)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
