// ignore_for_file: avoid_print
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import '../layout/footer.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PageController _pageController = PageController();
  int _currentPage = 0;
  RangeValues _currentLend = const RangeValues(0, 250000);
  RangeValues _currentLendDate = const RangeValues(0, 15);

  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final double _interest = 0.015;
  double _lend = 0;
  double _intPers = 0;

  void calc() {
    _lend = _currentLend.end * _interest * _currentLendDate.end * 0.1 +
        _currentLend.end;

    _intPers = _interest * 100;
  }

  Future<void> sendLoanRequest() async {
    String? accessToken = await _secureStorage.read(key: 'access_token');

    DateTime now = DateTime.now();
    String loanDate = DateFormat('dd-MMM-yyyy').format(now);

    DateTime due = now.add(Duration(days: _currentLendDate.end.toInt()));
    String loanDue = DateFormat('dd-MMM-yyyy').format(due);

    final response = await http.post(
      Uri.parse('https://system.edata.mn/ords/monterosa/mobile_dev/loan'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $accessToken',
      },
      body: jsonEncode(<String, dynamic>{
        'l_date': loanDate,
        'l_due': loanDue,
        'l_value': _currentLend.end.toInt(),
        'empno': 9999,
        'l_type': 1,
        'l_interest': _intPers,
      }),
    );

    if (response.statusCode == 200) {
      print('Loan request status: ${response.body}');
      showDialog(
        // ignore: use_build_context_synchronously
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: const Text('Таны хүсэлт амжилттай илгээгдлээ!'),
            actions: <Widget>[
              TextButton(
                child: const Text('За'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      throw Exception('Failed to send loan request');
    }
  }

  void _showInputDialog(BuildContext context, String title, String hintText,
      Function(String) onSubmitted) {
    TextEditingController controller = TextEditingController();

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: TextField(
            controller: controller,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(hintText: hintText),
            onSubmitted: onSubmitted,
          ),
          actions: [
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('OK'),
              onPressed: () {
                onSubmitted(controller.text);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Footer(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFFFFFFFF),
              Color(0xFF99FF66),
            ],
          ),
        ),
        child: Column(
          children: [
            Container(
              height: 190,
              margin: const EdgeInsets.fromLTRB(20, 50, 20, 5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: PageView(
                  controller: _pageController,
                  onPageChanged: (int page) {
                    setState(() {
                      _currentPage = page;
                    });
                  },
                  children: [
                    Image.asset('lib/public/th.jpg', fit: BoxFit.cover),
                    Image.asset('lib/public/th (1).jpg', fit: BoxFit.cover),
                    Image.asset('lib/public/th (2).jpg', fit: BoxFit.cover),
                  ],
                ),
              ),
            ),
            _buildPageIndicator(),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.width,
                    width: 320,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 10,
                          offset: Offset(0, 5),
                        ),
                      ],
                    ),
                    margin: const EdgeInsets.symmetric(horizontal: 30.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          'Шуурхай зээл',
                          style: TextStyle(
                            color: Color(0xFF416D19),
                            fontSize: 20,
                          ),
                        ),
                        const SizedBox(height: 20),
                        GestureDetector(
                          onTap: () {
                            _showInputDialog(
                              context,
                              'Мөнгөн дүнгээ оруулна уу.',
                              '',
                              (value) {
                                setState(() {
                                  _currentLend = RangeValues(
                                      _currentLend.start, double.parse(value));
                                  calc();
                                });
                              },
                            );
                          },
                          child: Text(
                            '${_currentLend.end.toInt()}₮',
                            style: const TextStyle(
                              color: Color(0xFF416D19),
                            ),
                          ),
                        ),
                        RangeSlider(
                          values: _currentLend,
                          min: 0,
                          max: 500000,
                          divisions: 10,
                          labels: RangeLabels(
                            _formatValue(_currentLend.start),
                            _formatValue(_currentLend.end),
                          ),
                          onChanged: (RangeValues values) {
                            setState(() {
                              _currentLend = values;
                              calc();
                            });
                          },
                        ),
                        GestureDetector(
                          onTap: () {
                            _showInputDialog(
                              context,
                              'Зээлэх хугацаагаа оруулна уу.',
                              '(Өдрөөр)',
                              (value) {
                                setState(() {
                                  _currentLendDate = RangeValues(
                                      _currentLendDate.start,
                                      double.parse(value));
                                  calc();
                                });
                              },
                            );
                          },
                          child: Text(
                            '${_currentLendDate.end.toInt()} хоног',
                            style: const TextStyle(
                              color: Color(0xFF416D19),
                            ),
                          ),
                        ),
                        RangeSlider(
                          values: _currentLendDate,
                          min: 0,
                          max: 30,
                          divisions: 30,
                          labels: RangeLabels(
                            _currentLendDate.start.round().toString(),
                            _currentLendDate.end.round().toString(),
                          ),
                          onChanged: (RangeValues values) {
                            setState(() {
                              _currentLendDate = values;
                              calc();
                            });
                          },
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.symmetric(horizontal: 30.0),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 50,
                                child: Text(
                                  'Хүү: $_intPers%',
                                  style: const TextStyle(
                                    color: Color(0xFF416D19),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 50,
                                child: Text(
                                  'Таны төлөх дүн: $_lend',
                                  style: const TextStyle(
                                    color: Color(0xFF416D19),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.symmetric(horizontal: 30.0),
                          decoration: const BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black26,
                                blurRadius: 20,
                                offset: Offset(0, 5),
                              ),
                            ],
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFF99FF66),
                              padding: const EdgeInsets.symmetric(
                                horizontal: 50,
                                vertical: 15,
                              ),
                            ),
                            onPressed: () {
                              sendLoanRequest();
                            },
                            child: const Text('Зээл авах'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _formatValue(double value) {
    return '${value.round()}₮';
  }

  Widget _buildPageIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(3, (int index) {
        return Container(
          width: 8.0,
          height: 8.0,
          margin: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: _currentPage == index
                ? const Color(0xFF99FF66)
                : const Color.fromARGB(255, 223, 216, 216),
          ),
        );
      }),
    );
  }
}
