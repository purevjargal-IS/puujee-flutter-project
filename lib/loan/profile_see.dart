import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/loan/profile_edit.dart';
import 'package:image_picker/image_picker.dart';
import '../layout/footer.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int _selectedIndex = 0;
  XFile? _imageFile;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  void _edit() {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => const ProfileEditScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Footer(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Color(0xFFFFFFFF),
              Color(0xFF99FF66),
            ],
          ),
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(50.0),
                    bottom: Radius.zero,
                  ),
                  color: Colors.lightGreen,
                ),
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(top: 150.0),
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    if (_imageFile != null)
                      CircleAvatar(
                        radius: 50,
                        backgroundImage: FileImage(File(_imageFile!.path)),
                      ),
                    const SizedBox(height: 20),
                    ElevatedButton(
                      onPressed: _edit,
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30, vertical: 10),
                        backgroundColor: const Color(0xFF416D19),
                        foregroundColor: Colors.white,
                      ),
                      child: const Text('Засварлах'),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
