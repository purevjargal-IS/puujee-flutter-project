// // ignore_for_file: avoid_print

// import 'dart:convert';
// import 'package:flutter/material.dart';
// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'package:provider/provider.dart';
// import 'package:http/http.dart' as http;
// import '../providers/auth_provider.dart';
// import 'delete.dart';
// import 'login.dart';
// import 'put.dart';

// class DataScreen extends StatefulWidget {
//   const DataScreen({super.key});

//   @override
//   // ignore: library_private_types_in_public_api
//   _DataScreenState createState() => _DataScreenState();
// }

// class _DataScreenState extends State<DataScreen> {
//   final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();
//   List<dynamic>? _data;

//   @override
//   void initState() {
//     super.initState();
//     _fetchData();
//   }

//   Future<void> _fetchData() async {
//     try {
//       String? accessToken = await _secureStorage.read(key: 'access_token');
//       if (accessToken != null) {
//         final response = await http.get(
//           Uri.parse('https://system.edata.mn/ords/monterosa/mobile_dev/emp'),
//           headers: <String, String>{
//             'Authorization': 'Bearer $accessToken',
//           },
//         );

//         if (response.statusCode == 200) {
//           var responseData = jsonDecode(response.body);
//           setState(() {
//             _data = responseData['items'];
//           });
//         } else {
//           print('Failed to fetch data: ${response.reasonPhrase}');
//         }
//       } else {
//         print('Access token not found');
//         _redirectToLogin();
//       }
//     } catch (e) {
//       print('Exception occurred: $e');
//     }
//   }

//   void _route(Widget screen) {
//     if (Provider.of<AuthProvider>(context, listen: false).isLoggedIn) {
//       Navigator.push(
//         context,
//         MaterialPageRoute(builder: (context) => screen),
//       );
//     } else {
//       _redirectToLogin();
//     }
//   }

//   void _redirectToLogin() {
//     Navigator.pushReplacement(
//       context,
//       MaterialPageRoute(builder: (context) => const LoginScreen()),
//     );
//   }

//   void _logout() {
//     Provider.of<AuthProvider>(context, listen: false).logout();
//     _redirectToLogin();
//   }

//   String filterName = '';
//   List<dynamic>? filtered;

//   void filterByName(String ename) {
//     setState(() {
//       filterName = ename;
//       filtered = _data!.where((data) => data.ename.toString().contains(filterName)).toList();
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Data Screen'),
//         actions: <Widget>[
//           IconButton(onPressed: _logout, icon: const Icon(Icons.logout)),
//         ],
//       ),
//       body: _data != null
//           ? Column(
//               children: [
//                 Expanded(
//                   child: ListView.builder(
//                     itemCount: _data!.length,
//                     itemBuilder: (context, index) {
//                       var item = _data![index];
//                       return ListTile(
//                         title: Text(item['ename']),
//                         subtitle:
//                             Text('Job: ${item['job']}\nSalary: ${item['sal']}'),
//                       );
//                     },
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.all(16.0),
//                   child: ElevatedButton(
//                     onPressed: () => _route(const PutScreenState()),
//                     style: ElevatedButton.styleFrom(
//                       backgroundColor: Colors.lightGreen[700],
//                       padding: const EdgeInsets.symmetric(
//                           horizontal: 50, vertical: 20),
//                     ),
//                     child: const Text(
//                       'Update',
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontSize: 20,
//                         fontWeight: FontWeight.bold,
//                       ),
//                     ),
//                   ),
//                 ),
//                 Padding(
//                   padding: const EdgeInsets.all(16.0),
//                   child: ElevatedButton(
//                     onPressed: () => _route(const DeleteScreenState()),
//                     style: ElevatedButton.styleFrom(
//                       backgroundColor: Colors.red,
//                       padding: const EdgeInsets.symmetric(
//                           horizontal: 50, vertical: 20),
//                     ),
//                     child: const Text(
//                       'Delete',
//                       style: TextStyle(
//                         color: Colors.white,
//                         fontSize: 20,
//                         fontWeight: FontWeight.bold,
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             )
//           : const Center(
//               child: CircularProgressIndicator(),
//             ),
//     );
//   }
// }


// ignore_for_file: avoid_print

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import '../providers/auth_provider.dart';
import 'delete.dart';
import 'login.dart';
import 'put.dart';
class DataScreen extends StatefulWidget {
  const DataScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _DataScreenState createState() => _DataScreenState();
}

class _DataScreenState extends State<DataScreen> {
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();
  List<dynamic>? _data;
  String filterName = '';
  List<dynamic>? filtered;

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  Future<void> _fetchData() async {
    try {
      String? accessToken = await _secureStorage.read(key: 'access_token');
      if (accessToken != null) {
        final response = await http.get(
          Uri.parse('https://system.edata.mn/ords/monterosa/mobile_dev/emp'),
          headers: <String, String>{
            'Authorization': 'Bearer $accessToken',
          },
        );

        if (response.statusCode == 200) {
          var responseData = jsonDecode(response.body);
          setState(() {
            _data = responseData['items'];
            filtered = _data;
          });
        } else {
          print('Failed to fetch data: ${response.reasonPhrase}');
        }
      } else {
        print('Access token not found');
        _redirectToLogin();
      }
    } catch (e) {
      print('Exception occurred: $e');
    }
  }

  void _route(Widget screen) {
    if (Provider.of<AuthProvider>(context, listen: false).isLoggedIn) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => screen),
      );
    } else {
      _redirectToLogin();
    }
  }

  void _redirectToLogin() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const LoginScreen()),
    );
  }

  void _logout() {
    Provider.of<AuthProvider>(context, listen: false).logout();
    _redirectToLogin();
  }

  void filterByName(String ename) {
    setState(() {
      filterName = ename;
      filtered = _data!.where((data) => data['ename'].toString().contains(filterName)).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Data Screen'),
        actions: <Widget>[
          IconButton(onPressed: _logout, icon: const Icon(Icons.logout)),
        ],
      ),
      body: _data != null
          ? Column(
              children: [
                TextField(
                  onChanged: filterByName,
                  decoration: const InputDecoration(
                    labelText: 'Filter by name',
                    hintText: 'Enter name to filter',
                    prefixIcon: Icon(Icons.search),
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                    itemCount: filtered!.length,
                    itemBuilder: (context, index) {
                      var item = filtered![index];
                      return ListTile(
                        title: Text(item['ename']),
                        subtitle:
                            Text('Job: ${item['job']}\nSalary: ${item['sal']}'),
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ElevatedButton(
                    onPressed: () => _route(const PutScreenState()),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.lightGreen[700],
                      padding: const EdgeInsets.symmetric(
                          horizontal: 50, vertical: 20),
                    ),
                    child: const Text(
                      'Update',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ElevatedButton(
                    onPressed: () => _route(const DeleteScreenState()),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 50, vertical: 20),
                    ),
                    child: const Text(
                      'Delete',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            )
          : const Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
