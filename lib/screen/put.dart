// ignore_for_file: use_build_context_synchronously, avoid_print, library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'test.dart';

class PutScreenState extends StatefulWidget {
  const PutScreenState({super.key});

  @override
  _PutScreenState createState() => _PutScreenState();
}

class _PutScreenState extends State<PutScreenState> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _empno = TextEditingController();
  final TextEditingController _ename = TextEditingController();
  final TextEditingController _job = TextEditingController();
  final TextEditingController _sal = TextEditingController();
  final TextEditingController _deptno = TextEditingController();
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  Future<void> _update() async {
    if (_formKey.currentState?.validate() ?? false) {
      String? accessToken = await _secureStorage.read(key: 'access_token');

      if (accessToken != null) {
        final response = await http.put(
          Uri.parse('https://system.edata.mn/ords/monterosa/mobile_dev/emp/${_empno.text}'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, String>{
            "empno": _empno.text,
            "ename": _ename.text,
            "job": _job.text,
            "sal": _sal.text,
            "deptno": _deptno.text,
          }),
        );

        if (response.statusCode == 200) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Successful!')),
          );
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const DataScreen()),
          );
          print(response.body);
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Failed!')),
          );
          print(response.statusCode);
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Access token not found')),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'POST',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.lightGreen[700],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.mobile_friendly_rounded,
                size: MediaQuery.of(context).size.height * 0.2,
                color: Colors.lightGreen[700],
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _empno,
                decoration: const InputDecoration(labelText: 'Employee Number'),
                keyboardType: TextInputType.number,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _ename,
                decoration: const InputDecoration(labelText: 'Name'),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _job,
                decoration: const InputDecoration(labelText: 'Job'),
                keyboardType: TextInputType.text,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _sal,
                decoration: const InputDecoration(labelText: 'Salary'),
                keyboardType: TextInputType.number,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _deptno,
                decoration: const InputDecoration(labelText: 'Department Number'),
                keyboardType: TextInputType.number,
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: _update,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.orange,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'Update',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
