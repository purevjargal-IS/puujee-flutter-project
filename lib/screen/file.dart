// ignore_for_file: avoid_print, library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'dart:io';
import '../layout/footer.dart';
import '../layout/header.dart';

class FileUploadScreen extends StatefulWidget {
  const FileUploadScreen({super.key});

  @override
  _FileUploadScreenState createState() => _FileUploadScreenState();
}

class _FileUploadScreenState extends State<FileUploadScreen> {
  int _selectedIndex = 0;
  File? _selectedFile;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<void> _pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    if (result != null) {
      String? filePath = result.files.single.path;

      if (filePath != null) {
        setState(() {
          _selectedFile = File(filePath);
        });
      }
    }
  }

  Future<void> _uploadFile() async {
    if (_selectedFile == null) return;
    const String apiUrl =
        'https://system.edata.mn/ords/monterosa/turshilt/fileAsByte';
    List<int> fileBytes = await _selectedFile!.readAsBytes();
    print(fileBytes);
    print(_selectedFile!.path.split('/').last);

    String mimeType =
        lookupMimeType(_selectedFile!.path) ?? 'application/octet-stream';
    print(mimeType);
    var request = http.MultipartRequest('POST', Uri.parse(apiUrl));

    request.files.add(http.MultipartFile.fromBytes(
      'file',
      fileBytes,
      filename: _selectedFile!.path.split('/').last,
      contentType: MediaType.parse(mimeType),
    ));
    try {
      http.StreamedResponse response = await request.send();
      var responseData = await http.Response.fromStream(response);
      if (response.statusCode == 200) {
        print('File uploaded successfully');
      } else {
        print('File upload failed with status: ${responseData.statusCode}');
        print('Response body: ${responseData.body}');
        print('Response headers: ${responseData.headers}');
      }
    } catch (e) {
      print('Error uploading file: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: CustomHeader(
          title: 'Хэрэглэгчийн мэдээлэл',
          onNotificationPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Alert'),
                  content: const Text('Button pressed'),
                  actions: <Widget>[
                    TextButton(
                      child: const Text('OK'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
      bottomNavigationBar: Footer(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      // body: Column(
      //   children: <Widget>[
      //     Expanded(
      //       child: Container(
      //         decoration: const BoxDecoration(
      //           borderRadius: BorderRadius.vertical(
      //             top: Radius.circular(50.0),
      //             bottom: Radius.zero,
      //           ),
      //           color: Colors.lightGreen,
      //         ),
      //         width: MediaQuery.of(context).size.width,
      //         margin: const EdgeInsets.only(top: 100.0),
      //         child: const Column(
      //           children: [
      //             Positioned(
      //               top: -25,
      //               child: CircleAvatar(
      //                 radius: 50,
      //                 backgroundImage: AssetImage('lib/public/OIP.jpg'),
      //               ),
      //             ),
      //           ],
      //         ),
      //       ),
      //     ),
      //   ],
      // ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: _pickFile,
                child: const Text('Pick File'),
              ),
              const SizedBox(height: 20),
              _selectedFile != null
                  ? _buildFileWidget()
                  : const Text('No file selected'),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: _uploadFile,
                child: const Text('Upload'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildFileWidget() {
    return Column(
      children: [
        Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width * 0.8,
            maxHeight: MediaQuery.of(context).size.height * 0.4,
          ),
          child: Image.file(_selectedFile!),
        ),
        const SizedBox(height: 10),
        Text(
          'Selected file: ${_selectedFile!.path.split('/').last}',
          style: const TextStyle(fontSize: 16),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
