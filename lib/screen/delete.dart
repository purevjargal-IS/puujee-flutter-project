// ignore_for_file: use_build_context_synchronously, avoid_print, library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'test.dart';

class DeleteScreenState extends StatefulWidget {
  const DeleteScreenState({super.key});

  @override
  _DeleteScreenState createState() => _DeleteScreenState();
}

class _DeleteScreenState extends State<DeleteScreenState> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _empno = TextEditingController();
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  Future<void> _delete() async {
    if (_formKey.currentState?.validate() ?? false) {
      String? accessToken = await _secureStorage.read(key: 'access_token');

      if (accessToken != null) {
        final response = await http.delete(
          Uri.parse(
              'https://system.edata.mn/ords/monterosa/mobile_dev/emp/${_empno.text}'),
          headers: <String, String>{
            'Authorization': 'Bearer $accessToken',
          },
          body: jsonEncode(<String, String>{
            'empno': _empno.text,
          }),
        );

        if (response.statusCode == 200) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Successful!')),
          );
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const DataScreen()),
          );
          print(response.body);
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Failed!')),
          );
          print(response.statusCode);
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Access token not found')),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Delete Employee',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.lightGreen[700],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.mobile_friendly_rounded,
                size: MediaQuery.of(context).size.height * 0.2,
                color: Colors.lightGreen[700],
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _empno,
                decoration: const InputDecoration(labelText: 'Employee Number'),
                keyboardType: TextInputType.number,
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: _delete,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'Delete',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
