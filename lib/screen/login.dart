// // ignore_for_file: avoid_print, use_build_context_synchronously

// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';
// //import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'dart:convert';
// import 'home.dart';
// //import 'package:shared_preferences/shared_preferences.dart';

// import 'registration.dart';

// class LoginScreen extends StatefulWidget {
//   const LoginScreen({super.key});

//   @override
//   // ignore: library_private_types_in_public_api
//   _LoginScreenState createState() => _LoginScreenState();
// }

// class _LoginScreenState extends State<LoginScreen> {
//   final _formKey = GlobalKey<FormState>();
//   final TextEditingController _phoneController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();

//   String? _validatePhone(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Утасны дугаараа оруулна уу!';
//     }
//     final phoneRegex = RegExp(r'^\d{8}$');
//     if (!phoneRegex.hasMatch(value)) {
//       return 'Бүртгэлтэй утасны дугаар оруулна уу!';
//     }
//     return null;
//   }

//   void _route() {
//     Navigator.push(
//       context,
//       MaterialPageRoute(builder: (context) => const RegistrationScreen()),
//     );
//   }

//   Future<void> _getUserData() async {
//     final response = await http.get(
//       Uri.parse(
//           'https://system.edata.mn/ords/monterosa/flutter/login/${_phoneController.text}/${_passwordController.text}'),
//       headers: <String, String>{
//         'Content-Type': 'application/json; charset=UTF-8',
//       },
//     );

//     if (response.statusCode == 200) {
//       var data = jsonDecode(response.body);
//       print('User Data: $data');

//       SharedPreferences prefs = await SharedPreferences.getInstance();
//       await prefs.setString('phone_number', _phoneController.text);
//       await prefs.setString('password', _passwordController.text);

//       Navigator.pushReplacement(
//         context,
//         MaterialPageRoute(builder: (context) => const HomeScreen()),
//       );
//     } else {
//       print('Failed to load user data');
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Нэвтрэх',
//             style: TextStyle(
//               color: Colors.white,
//             )),
//         backgroundColor: Colors.lightGreen[700],
//       ),
//       body: SingleChildScrollView(
//         padding: const EdgeInsets.all(16.0),
//         child: Form(
//           key: _formKey,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Icon(
//                 Icons.mobile_friendly_rounded,
//                 size: MediaQuery.of(context).size.height * 0.2,
//                 color: Colors.lightGreen[700],
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _phoneController,
//                 decoration: const InputDecoration(labelText: 'Утасны дугаар'),
//                 keyboardType: TextInputType.phone,
//                 validator: _validatePhone,
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _passwordController,
//                 decoration: const InputDecoration(labelText: 'Нууц үг'),
//                 obscureText: true,
//               ),
//               const SizedBox(height: 30),
//               ElevatedButton(
//                 onPressed: _getUserData,
//                 style: ElevatedButton.styleFrom(
//                   backgroundColor: Colors.lightGreen[700],
//                   padding:
//                       const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
//                 ),
//                 child: const Text(
//                   'Нэвтрэх',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 20,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//               ),
//               const SizedBox(height: 30),
//               ElevatedButton(
//                 onPressed: _route,
//                 style: ElevatedButton.styleFrom(
//                   backgroundColor: Colors.lightGreen[500],
//                   padding:
//                       const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
//                 ),
//                 child: const Text(
//                   'Бүртгүүлэх',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 20,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }


// ignore_for_file: avoid_print, use_build_context_synchronously, library_private_types_in_public_api
import 'package:flutter/material.dart';
//import 'package:flutter_application_1/screen/test.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:convert';
import 'home.dart';
import 'post.dart';
//import 'registration.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  String? _validatePhone(String? value) {
    if (value == null || value.isEmpty) {
      return 'Хэрэглэгчийн нэрээ оруулна уу!';
    }
    return null;
  }

  void _post() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const PostScreenState()),
    );
  }

  Future<void> _getToken(String phoneNumber, String password) async {
    try {
      final response = await http.post(
        Uri.parse('https://system.edata.mn/ords/monterosa/public/get_token'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'username': phoneNumber,
          'password': password,
        }),
      );

      if (response.statusCode == 200) {
        var data = jsonDecode(response.body);
        var token = data['token'];
        print('Access token: $token');

        await _secureStorage.write(key: 'access_token', value: token);
        //await _secureStorage.write(key: 'empno', value: phoneNumber);

        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const HomeScreen()),
          //MaterialPageRoute(builder: (context) => const DataScreen()),
        );
      } else {
        print('Failed to get access token');
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Failed to login. Please try again.'),
          ),
        );
      }
    } catch (e) {
      print('Exception occurred: $e');
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content:
              Text('An unexpected error occurred. Please try again later.'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Нэвтрэх эрх авах',
            style: TextStyle(
              color: Colors.white,
            )),
        backgroundColor: Colors.lightGreen[700],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.mobile_friendly_rounded,
                size: MediaQuery.of(context).size.height * 0.2,
                color: Colors.lightGreen[700],
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _phoneController,
                decoration: const InputDecoration(labelText: 'Хэрэглэгчийн нэр'),
                keyboardType: TextInputType.phone,
                validator: _validatePhone,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _passwordController,
                decoration: const InputDecoration(labelText: 'Нууц үг'),
                obscureText: true,
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Түр хүлээнэ үү...'),
                            CircularProgressIndicator(),
                          ],
                        ),
                      ),
                    );
                    _getToken(_phoneController.text, _passwordController.text);
                  }
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.lightGreen[700],
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'Нэвтрэх эрх авах',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: _post,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.lightGreen[500],
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'POST - INSERT',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
