// ignore_for_file: avoid_print
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
//import 'package:provider/provider.dart';
import '../layout/footer.dart';
import '../layout/header.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PageController _pageController = PageController();
  int _currentPage = 0;
  RangeValues _currentLend = const RangeValues(0, 100000);
  RangeValues _currentLendDate = const RangeValues(0, 15);

  final FlutterSecureStorage _secureStorage = const FlutterSecureStorage();

  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final double _interest = 0.15;
  double _lend = 0;

  void calc() {
    _lend = _currentLend.end * _interest * _currentLendDate.end * 0.1 +
        _currentLend.end;
  }

  // Future<void> sendLoanRequest() async {
  //   String? accessToken = await _secureStorage.read(key: 'access_token');
  //   DateTime now = DateTime.now();
  //   String loanDate =
  //       '${now.year}-${_formatDatePart(now.month)}-${_formatDatePart(now.day)}';
  //   DateTime due = now.add(Duration(days: _currentLendDate.end.toInt()));
  //   String loanDue =
  //       '${due.year}-${_formatDatePart(due.month)}-${_formatDatePart(due.day)}';
  //   final response = await http.post(
  //     Uri.parse('https://system.edata.mn/ords/monterosa/mobile_dev/loan'),
  //     headers: <String, String>{
  //       'Content-Type': 'application/json',
  //       'Authorization': 'Bearer $accessToken',
  //     },
  //     body: jsonEncode(<String, dynamic>{
  //       'l_date': loanDate,
  //       'l_due': loanDue,
  //       'l_value': _currentLend.end.toInt(),
  //       'empno': 9999,
  //       'l_type': 1,
  //       'l_interest': _interest,
  //     }),
  //   );
  //   if (response.statusCode == 200) {
  //     final result = response.body;
  //     print('Loan request status: $result');
  //   } else {
  //     throw Exception('Failed to send loan request');
  //   }
  // }

  Future<void> sendLoanRequest() async {
    String? accessToken = await _secureStorage.read(key: 'access_token');

    DateTime now = DateTime.now();
    String loanDate = DateFormat('dd-MMM-yyyy').format(now);

    DateTime due = now.add(Duration(days: _currentLendDate.end.toInt()));
    String loanDue = DateFormat('dd-MMM-yyyy').format(due);

    final response = await http.post(
      Uri.parse('https://system.edata.mn/ords/monterosa/mobile_dev/loan'),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $accessToken',
      },
      body: jsonEncode(<String, dynamic>{
        'l_date': loanDate,
        'l_due': loanDue,
        'l_value': _currentLend.end.toInt(),
        'empno': 9999,
        'l_type': 1,
        'l_interest': _interest,
      }),
    );

    if (response.statusCode == 200) {
      print('Loan request status: ${response.body}');
      showDialog(
        // ignore: use_build_context_synchronously
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: const Text('Таны хүсэлт амжилттай илгээгдлээ!'),
            actions: <Widget>[
              TextButton(
                child: const Text('За'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      throw Exception('Failed to send loan request');
    }
  }

  // String _formatDatePart(int datePart) {
  //   return datePart < 10 ? '0$datePart' : '$datePart';
  // }
  // String _formatDatePart(int datePart) {
  //   String monthAbbreviation =
  //       DateFormat('MMM').format(DateTime(2024, datePart, 1));
  //   return monthAbbreviation.toUpperCase();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: CustomHeader(
          title: 'Шуурхай зээл',
          onNotificationPressed: () {
            setState(() {
              calc();
            });
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Alert'),
                  content: const Text('Button pressed'),
                  actions: <Widget>[
                    TextButton(
                      child: const Text('OK'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
      bottomNavigationBar: Footer(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10.0),
                child: PageView(
                  controller: _pageController,
                  onPageChanged: (int page) {
                    setState(() {
                      _currentPage = page;
                    });
                  },
                  children: [
                    Image.asset('lib/public/th.jpg'),
                    Image.asset('lib/public/th (1).jpg'),
                    Image.asset('lib/public/th (2).jpg'),
                  ],
                ),
              ),
            ),
          ),
          _buildPageIndicator(),
          Expanded(
            flex: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.grey[400],
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  margin: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text('Зээлийн мөнгөн дүн:'),
                      RangeSlider(
                        values: _currentLend,
                        min: 0,
                        max: 200000,
                        divisions: 200,
                        labels: RangeLabels(
                          _formatValue(_currentLend.start),
                          _formatValue(_currentLend.end),
                        ),
                        onChanged: (RangeValues values) {
                          setState(() {
                            _currentLend = values;
                            calc();
                          });
                        },
                      ),
                      const Text('Зээлийн хугацаа:'),
                      RangeSlider(
                        values: _currentLendDate,
                        min: 0,
                        max: 30,
                        divisions: 30,
                        labels: RangeLabels(
                          _currentLendDate.start.round().toString(),
                          _currentLendDate.end.round().toString(),
                        ),
                        onChanged: (RangeValues values) {
                          setState(() {
                            _currentLendDate = values;
                            calc();
                          });
                        },
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.grey[400],
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: 70,
                  margin: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: Center(
                    child: Text('Таны төлөх дүн: $_lend'),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.symmetric(horizontal: 30.0),
                  child: ElevatedButton(
                    // onPressed: () {
                    //   showDialog(
                    //     context: context,
                    //     builder: (BuildContext context) {
                    //       return AlertDialog(
                    //         title: const Text('Alert'),
                    //         content: const Text('Button pressed'),
                    //         actions: <Widget>[
                    //           TextButton(
                    //             child: const Text('OK'),
                    //             onPressed: () {
                    //               Navigator.of(context).pop();
                    //             },
                    //           ),
                    //         ],
                    //       );
                    //     },
                    //   );
                    // },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.lightGreen[700],
                      padding: const EdgeInsets.symmetric(
                        horizontal: 50,
                        vertical: 15,
                      ),
                    ),
                    onPressed: () {
                      sendLoanRequest();
                    },
                    child: const Text('Зээл авах'),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _formatValue(double value) {
    return '${value.round()}₮';
  }

  Widget _buildPageIndicator() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(3, (int index) {
        return Container(
          width: 8.0,
          height: 8.0,
          margin: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: _currentPage == index
                ? Colors.lightGreen[700]
                : Colors.grey[400],
          ),
        );
      }),
    );
  }
}
