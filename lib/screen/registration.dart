// import 'package:flutter/material.dart';
// import 'login.dart';

// class RegistrationScreen extends StatefulWidget {
//   const RegistrationScreen({super.key});

//   @override
//   // ignore: library_private_types_in_public_api
//   _RegistrationScreenState createState() => _RegistrationScreenState();
// }

// class _RegistrationScreenState extends State<RegistrationScreen> {
//   final _formKey = GlobalKey<FormState>();
//   final TextEditingController _phoneController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final TextEditingController _confirmPasswordController =
//       TextEditingController();

//   @override
//   void dispose() {
//     _phoneController.dispose();
//     _passwordController.dispose();
//     _confirmPasswordController.dispose();
//     super.dispose();
//   }

//   void _register() {
//     if (_formKey.currentState?.validate() ?? false) {
//       ScaffoldMessenger.of(context).showSnackBar(
//         const SnackBar(content: Text('Бүртгэл амжилттай!')),
//       );
//       Navigator.push(
//         context,
//         MaterialPageRoute(builder: (context) => const LoginScreen()),
//       );
//     }
//   }

//   String? _validatePhone(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Утасны дугаараа оруулна уу!';
//     }
//     final phoneRegex = RegExp(r'^\d{8}$');
//     if (!phoneRegex.hasMatch(value)) {
//       return 'Зөв утасны дугаар оруулна уу!';
//     }
//     return null;
//   }

//   String? _validatePassword(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Нууц үгээ оруулна уу!';
//     }
//     if (value.length < 6) {
//       return 'Нууц үг дор хаяж 6 тэмдэгт байх ёстой!';
//     }
//     return null;
//   }

//   String? _validateConfirmPassword(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Нууц үгээ дахин оруулна уу!';
//     }
//     if (value != _passwordController.text) {
//       return 'Нууц үг буруу байна!';
//     }
//     return null;
//   }

// ignore_for_file: use_build_context_synchronously, library_private_types_in_public_api, avoid_print

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Бүртгүүлэх',
//             style: TextStyle(
//               color: Colors.white,
//             )),
//         backgroundColor: Colors.lightGreen[700],
//       ),
//       body: SingleChildScrollView(
//         padding: const EdgeInsets.all(16.0),
//         child: Form(
//           key: _formKey,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Icon(
//                 Icons.mobile_friendly_rounded,
//                 size: MediaQuery.of(context).size.height * 0.2,
//                 color: Colors.lightGreen[700],
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _phoneController,
//                 decoration: const InputDecoration(labelText: 'Утасны дугаар'),
//                 keyboardType: TextInputType.phone,
//                 validator: _validatePhone,
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _passwordController,
//                 decoration: const InputDecoration(labelText: 'Нууц үг'),
//                 obscureText: true,
//                 validator: _validatePassword,
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _confirmPasswordController,
//                 decoration: const InputDecoration(labelText: 'Дахин давтах'),
//                 obscureText: true,
//                 validator: _validateConfirmPassword,
//               ),
//               const SizedBox(height: 30),
//               ElevatedButton(
//                 onPressed: _register,
//                 style: ElevatedButton.styleFrom(
//                   backgroundColor: Colors.lightGreen[700],
//                   padding:
//                       const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
//                 ),
//                 child: const Text(
//                   'Бүртгүүлэх',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 20,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';
// import 'login.dart';
// import 'package:shared_preferences/shared_preferences.dart';

// class RegistrationScreen extends StatefulWidget {
//   const RegistrationScreen({super.key});

//   @override
//   _RegistrationScreenState createState() => _RegistrationScreenState();
// }

// class _RegistrationScreenState extends State<RegistrationScreen> {
//   final _formKey = GlobalKey<FormState>();
//   final TextEditingController _phoneController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final TextEditingController _confirmPasswordController =
//       TextEditingController();

//   String? _validatePhone(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Утасны дугаараа оруулна уу!';
//     }
//     final phoneRegex = RegExp(r'^\d{8}$');
//     if (!phoneRegex.hasMatch(value)) {
//       return 'Та утасны дугаараа шалгана уу!';
//     }
//     return null;
//   }

//   String? _validatePassword(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Нууц үгээ оруулна уу!';
//     }
//     if (value.length < 6) {
//       return 'Дор хаяж 6 тэмдэгт байх ёстой!';
//     }
//     return null;
//   }

//   String? _validateConfirmPassword(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Нууц үгээ давтана уу!';
//     }
//     if (value != _passwordController.text) {
//       return 'Нууц үг тохирохгүй байна!';
//     }
//     return null;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text(
//           'Бүртгүүлэх',
//           style: TextStyle(
//             color: Colors.white,
//           ),
//         ),
//         backgroundColor: Colors.lightGreen[700],
//       ),
//       body: SingleChildScrollView(
//         padding: const EdgeInsets.all(16.0),
//         child: Form(
//           key: _formKey,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Icon(
//                 Icons.mobile_friendly_rounded,
//                 size: MediaQuery.of(context).size.height * 0.2,
//                 color: Colors.lightGreen[700],
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _phoneController,
//                 decoration: const InputDecoration(labelText: 'Утасны дугаар'),
//                 keyboardType: TextInputType.phone,
//                 validator: _validatePhone,
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _passwordController,
//                 decoration: const InputDecoration(labelText: 'Нууц үг'),
//                 obscureText: true,
//                 validator: _validatePassword,
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _confirmPasswordController,
//                 decoration:
//                     const InputDecoration(labelText: 'Нууц үг баталгаажуулах'),
//                 obscureText: true,
//                 validator: _validateConfirmPassword,
//               ),
//               const SizedBox(height: 30),
//               ElevatedButton(
//                 onPressed: _register,
//                 style: ElevatedButton.styleFrom(
//                   backgroundColor: Colors.lightGreen[700],
//                   padding:
//                       const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
//                 ),
//                 child: const Text(
//                   'Бүртгүүлэх',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 20,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   Future<void> _register() async {
//     if (_formKey.currentState?.validate() ?? false) {
//       final response = await http.post(
//         Uri.parse('https://test.cgf.mn/ords/devenv/public/get_token'),
//         headers: <String, String>{
//           'Content-Type': 'application/json; charset=UTF-8',
//         },
//         body: jsonEncode(<String, String>{
//           'username': _phoneController.text,
//           'password': _passwordController.text,
//         }),
//       );

//       if (response.statusCode == 200) {
//         SharedPreferences prefs = await SharedPreferences.getInstance();
//         await prefs.setString('username', _phoneController.text);
//         await prefs.setString('password', _passwordController.text);

//         ScaffoldMessenger.of(context).showSnackBar(
//           const SnackBar(content: Text('Registration Successful!')),
//         );
//         Navigator.push(
//           context,
//           MaterialPageRoute(builder: (context) => const LoginScreen()),
//         );
//       } else {
//         ScaffoldMessenger.of(context).showSnackBar(
//           const SnackBar(content: Text('Registration Failed!')),
//         );
//       }
//     }
//   }
// }

// import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'login.dart';

// class RegistrationScreen extends StatefulWidget {
//   const RegistrationScreen({super.key});

//   @override
//   _RegistrationScreenState createState() => _RegistrationScreenState();
// }

// class _RegistrationScreenState extends State<RegistrationScreen> {
//   final _formKey = GlobalKey<FormState>();
//   final TextEditingController _phoneController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final TextEditingController _confirmPasswordController = TextEditingController();

//   @override
//   void dispose() {
//     _phoneController.dispose();
//     _passwordController.dispose();
//     _confirmPasswordController.dispose();
//     super.dispose();
//   }

//   Future<void> _register() async {
//     if (_formKey.currentState?.validate() ?? false) {
//       // Save user information locally using SharedPreferences
//       SharedPreferences prefs = await SharedPreferences.getInstance();
//       await prefs.setString('username', _phoneController.text);
//       await prefs.setString('password', _passwordController.text);

//       ScaffoldMessenger.of(context).showSnackBar(
//         const SnackBar(content: Text('Registration Successful!')),
//       );

//       Navigator.push(
//         context,
//         MaterialPageRoute(builder: (context) => const LoginScreen()),
//       );
//     }
//   }

//   String? _validatePhone(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Please enter your phone number!';
//     }
//     final phoneRegex = RegExp(r'^\d{8}$');
//     if (!phoneRegex.hasMatch(value)) {
//       return 'Please enter a valid phone number!';
//     }
//     return null;
//   }

//   String? _validatePassword(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Please enter your password!';
//     }
//     if (value.length < 6) {
//       return 'Password must be at least 6 characters!';
//     }
//     return null;
//   }

//   String? _validateConfirmPassword(String? value) {
//     if (value == null || value.isEmpty) {
//       return 'Please confirm your password!';
//     }
//     if (value != _passwordController.text) {
//       return 'Passwords do not match!';
//     }
//     return null;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Register',
//             style: TextStyle(
//               color: Colors.white,
//             )),
//         backgroundColor: Colors.lightGreen[700],
//       ),
//       body: SingleChildScrollView(
//         padding: const EdgeInsets.all(16.0),
//         child: Form(
//           key: _formKey,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Icon(
//                 Icons.mobile_friendly_rounded,
//                 size: MediaQuery.of(context).size.height * 0.2,
//                 color: Colors.lightGreen[700],
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _phoneController,
//                 decoration: const InputDecoration(labelText: 'Phone Number'),
//                 keyboardType: TextInputType.phone,
//                 validator: _validatePhone,
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _passwordController,
//                 decoration: const InputDecoration(labelText: 'Password'),
//                 obscureText: true,
//                 validator: _validatePassword,
//               ),
//               const SizedBox(height: 20),
//               TextFormField(
//                 controller: _confirmPasswordController,
//                 decoration: const InputDecoration(labelText: 'Confirm Password'),
//                 obscureText: true,
//                 validator: _validateConfirmPassword,
//               ),
//               const SizedBox(height: 30),
//               ElevatedButton(
//                 onPressed: _register,
//                 style: ElevatedButton.styleFrom(
//                   backgroundColor: Colors.lightGreen[700],
//                   padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
//                 ),
//                 child: const Text(
//                   'Register',
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontSize: 20,
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'login.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({super.key});

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  String? _validatePhone(String? value) {
    if (value == null || value.isEmpty) {
      return 'Утасны дугаараа оруулна уу!';
    }
    final phoneRegex = RegExp(r'^\d{8}$');
    if (!phoneRegex.hasMatch(value)) {
      return 'Та утасны дугаараа шалгана уу!';
    }
    return null;
  }

  String? _validatePassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'Нууц үгээ оруулна уу!';
    }
    if (value.length < 6) {
      return 'Дор хаяж 6 тэмдэгт байх ёстой!';
    }
    return null;
  }

  String? _validateConfirmPassword(String? value) {
    if (value == null || value.isEmpty) {
      return 'Нууц үгээ давтана уу!';
    }
    if (value != _passwordController.text) {
      return 'Нууц үг тохирохгүй байна!';
    }
    return null;
  }

  Future<void> _register() async {
    if (_formKey.currentState?.validate() ?? false) {
      final response = await http.post(
        Uri.parse('https://system.edata.mn/ords/monterosa/flutter/register'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'user_name': _usernameController.text,
          'phone_number': _phoneController.text,
          'password': _passwordController.text,
        }),
      );

      if (response.statusCode == 200) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('user_name', _usernameController.text);
        await prefs.setString('phone_number', _phoneController.text);
        await prefs.setString('password', _passwordController.text);

        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Registration Successful!')),
        );
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const LoginScreen()),
        );
        print(response.body);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Registration Failed!')),
        );
        print(response.statusCode);
      }
    }
  }

  Future<void> _getUserData() async {
    final response = await http.get(
      Uri.parse(
          'https://system.edata.mn/ords/monterosa/flutter/register/${_phoneController.text}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print('User Data: $data');
      setState(() {
        _usernameController.text = data['items'][0]['user_name'] ?? '';
        _phoneController.text = data['items'][0]['phone_number'] ?? '';
        _passwordController.text = data['items'][0]['password'] ?? '';
        _confirmPasswordController.text = data['items'][0]['password'] ?? '';
      });
    } else {
      print('Failed to load user data');
    }
  }

  Future<void> _updateUser() async {
    final response = await http.put(
      Uri.parse(
          'https://system.edata.mn/ords/monterosa/flutter/register/${_phoneController.text}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'user_name': _usernameController.text,
        'phone_number': _phoneController.text,
        'password': _passwordController.text,
      }),
    );

    if (response.statusCode == 200) {
      print('User Updated Successfully');
    } else {
      print('Failed to update user');
    }
  }

  Future<void> _deleteUser() async {
    final response = await http.delete(
      Uri.parse(
          'https://system.edata.mn/ords/monterosa/flutter/register/${_phoneController.text}'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'phone_number': _phoneController.text,
      }),
    );

    if (response.statusCode == 200) {
      print('User Deleted Successfully');
    } else {
      print('Failed to delete user');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Бүртгүүлэх',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.lightGreen[700],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.mobile_friendly_rounded,
                size: MediaQuery.of(context).size.height * 0.2,
                color: Colors.lightGreen[700],
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _usernameController,
                decoration: const InputDecoration(labelText: 'Нэр'),
                keyboardType: TextInputType.name,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _phoneController,
                decoration: const InputDecoration(labelText: 'Утасны дугаар'),
                keyboardType: TextInputType.phone,
                validator: _validatePhone,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _passwordController,
                decoration: const InputDecoration(labelText: 'Нууц үг'),
                obscureText: true,
                validator: _validatePassword,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _confirmPasswordController,
                decoration:
                    const InputDecoration(labelText: 'Нууц үг баталгаажуулах'),
                obscureText: true,
                validator: _validateConfirmPassword,
              ),
              const SizedBox(height: 30),
              ElevatedButton(
                onPressed: _register,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.lightGreen[700],
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'Бүртгүүлэх',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: _getUserData,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'Get User Data',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: _updateUser,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.orange,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'Update User',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: _deleteUser,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                child: const Text(
                  'Delete User',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
