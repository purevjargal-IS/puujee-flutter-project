// // ignore_for_file: library_private_types_in_public_api

// import 'dart:io';

// import 'package:file_picker/file_picker.dart';
// import 'package:flutter/material.dart';
// //import 'package:image_picker/image_picker.dart';
// import '../layout/footer.dart';
// import '../layout/header.dart';

// class ProfileScreen extends StatefulWidget {
//   const ProfileScreen({super.key});

//   @override
//   _ProfileScreenState createState() => _ProfileScreenState();
// }

// class _ProfileScreenState extends State<ProfileScreen> {
//   int _selectedIndex = 0;
//   File? _image;

//   void _onItemTapped(int index) {
//     setState(() {
//       _selectedIndex = index;
//     });
//   }

//   Future<void> _pickImage() async {
//     FilePickerResult? result = await FilePicker.platform.pickFiles();

//     if (result != null) {
//       String? filePath = result.files.single.path;

//       if (filePath != null) {
//         setState(() {
//           _image = File(filePath);
//         });
//       }
//     }
//   }

// ignore_for_file: library_private_types_in_public_api

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: PreferredSize(
//         preferredSize: const Size.fromHeight(50),
//         child: CustomHeader(
//           title: 'Хэрэглэгчийн мэдээлэл',
//           onNotificationPressed: () {
//             showDialog(
//               context: context,
//               builder: (BuildContext context) {
//                 return AlertDialog(
//                   title: const Text('Alert'),
//                   content: const Text('Button pressed'),
//                   actions: <Widget>[
//                     TextButton(
//                       child: const Text('OK'),
//                       onPressed: () {
//                         Navigator.of(context).pop();
//                       },
//                     ),
//                   ],
//                 );
//               },
//             );
//           },
//         ),
//       ),
//       bottomNavigationBar: Footer(
//         selectedIndex: _selectedIndex,
//         onItemTapped: _onItemTapped,
//       ),
//       body: Column(
//         children: <Widget>[
//           Expanded(
//             child: Container(
//               decoration: const BoxDecoration(
//                 borderRadius: BorderRadius.vertical(
//                   top: Radius.circular(50.0),
//                   bottom: Radius.zero,
//                 ),
//                 color: Colors.lightGreen,
//               ),
//               width: MediaQuery.of(context).size.width,
//               margin: const EdgeInsets.only(top: 100.0),
//               child: Column(
//                 children: [
//                   const SizedBox(height: 20),
//                   if (_image != null)
//                     CircleAvatar(
//                       radius: 50,
//                       backgroundImage: FileImage(_image!),
//                     ),
//                   const SizedBox(height: 20),
//                   ElevatedButton(
//                     onPressed: _pickImage,
//                     child: const Text('pick'),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../layout/footer.dart';
import '../layout/header.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int _selectedIndex = 0;
  XFile? _imageFile;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<void> _pickImage() async {
    final imagePicker = ImagePicker();
    final pickedFile = await imagePicker.pickImage(
      source: ImageSource.gallery,
    );

    if (pickedFile != null) {
      setState(() {
        _imageFile = pickedFile;
      });
    }
  }

  Future<void> _takeImage() async {
    final imagePicker = ImagePicker();
    final pickedFile = await imagePicker.pickImage(
      source: ImageSource.camera,
    );

    if (pickedFile != null) {
      setState(() {
        _imageFile = pickedFile;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: CustomHeader(
          title: 'Хэрэглэгчийн мэдээлэл',
          onNotificationPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Alert'),
                  content: const Text('Button pressed'),
                  actions: <Widget>[
                    TextButton(
                      child: const Text('OK'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
      bottomNavigationBar: Footer(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(50.0), 
                  bottom: Radius.zero,
                ),
                color: Colors.lightGreen,
              ),
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(top: 100.0),
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  if (_imageFile != null)
                    CircleAvatar(
                      radius: 50,
                      backgroundImage: FileImage(File(_imageFile!.path)),
                    ),
                  const SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: _pickImage,
                    child: const Text('pick'),
                  ),
                  const SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: _takeImage,
                    child: const Text('take picture'),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
