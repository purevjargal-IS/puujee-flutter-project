import 'package:flutter/material.dart';
import 'package:flutter_application_1/screen/test.dart';

class CustomHeader extends StatelessWidget {
  final String title;
  final Function()? onNotificationPressed;

  const CustomHeader({
    super.key,
    required this.title,
    this.onNotificationPressed,
  });

  void _displayDataScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const DataScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        title,
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
      backgroundColor: Colors.lightGreen[700],
      leading: Container(
        padding: const EdgeInsets.only(left: 10.0),
        child: const Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              Icons.mobile_friendly_rounded,
              color: Colors.white,
            ),
            SizedBox(width: 8),
            // Uncomment the icon if you need it
            // Icon(
            //   Icons.person,
            //   color: Colors.white,
            // ),
          ],
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: const Icon(
            Icons.person,
            color: Colors.white,
          ),
          onPressed: () => _displayDataScreen(context),
        ),
        IconButton(
          icon: const Icon(
            Icons.notifications,
            color: Colors.white,
          ),
          onPressed: onNotificationPressed,
        ),
      ],
    );
  }
}
